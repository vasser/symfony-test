<?php

namespace Lego\CatalogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductAttributes
 *
 * @ORM\Table(name="product_attributes", indexes={@ORM\Index(name="id_product", columns={"id_product"})})
 * @ORM\Entity
 */
class ProductAttributes
{
    /**
     * @var string
     *
     * @ORM\Column(name="pkey", type="string", length=255, nullable=false)
     */
    private $pkey;

    /**
     * @var string
     *
     * @ORM\Column(name="pvalue", type="string", length=255, nullable=false)
     */
    private $pvalue;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Lego\CatalogBundle\Entity\Product
     *
     * @ORM\ManyToOne(targetEntity="Lego\CatalogBundle\Entity\Product")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_product", referencedColumnName="id")
     * })
     */
    private $idProduct;



    /**
     * Set pkey
     *
     * @param string $pkey
     * @return ProductAttributes
     */
    public function setPkey($pkey)
    {
        $this->pkey = $pkey;

        return $this;
    }

    /**
     * Get pkey
     *
     * @return string 
     */
    public function getPkey()
    {
        return $this->pkey;
    }

    /**
     * Set pvalue
     *
     * @param string $pvalue
     * @return ProductAttributes
     */
    public function setPvalue($pvalue)
    {
        $this->pvalue = $pvalue;

        return $this;
    }

    /**
     * Get pvalue
     *
     * @return string 
     */
    public function getPvalue()
    {
        return $this->pvalue;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idProduct
     *
     * @param \Lego\CatalogBundle\Entity\Product $idProduct
     * @return ProductAttributes
     */
    public function setIdProduct(\Lego\CatalogBundle\Entity\Product $idProduct = null)
    {
        $this->idProduct = $idProduct;

        return $this;
    }

    /**
     * Get idProduct
     *
     * @return \Lego\CatalogBundle\Entity\Product 
     */
    public function getIdProduct()
    {
        return $this->idProduct;
    }
}
