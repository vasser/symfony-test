<?php
namespace Lego\CatalogBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ListingController extends Controller
{

    /**
     * Prepares home page
     * @return object
     */
    public function homeAction($page = 1)
    {
        if(empty($page)) {
            $page = 1;
        }

        $productsData = $this->get('lego_product')->getProductsAll($page);
        $products = $productsData['products'];
        $pager = $productsData['pager'];
        $categories = $this->get('lego_category')->getCategories();
        $orderList = $this->get('lego_product')->sortKeys;
        $currencies = $this->get('lego_product')->getCurrencies();

        return $this->render(
            'LegoCatalogBundle:Listing:products.html.twig', array(
                'data_prod' => $products,
                'pager_prod' => $pager,
                'categories' => $categories,
                'orderList' => $orderList,
                'currencies' => $currencies
            )
        );
    }

    /**
     * Prepares list of categories page
     * @return object
     */
    public function catalogsAction()
    {

        $categories = $this->get('lego_category')->getCategories();

        return $this->render(
            'LegoCatalogBundle:Listing:products.html.twig', array(
                'data_cat' => $categories,
                'categories' => $categories)
        );
    }

    /**
     * Prepares single category page
     * @param type $catalog ID of category to display
     * @return object
     */
    public function catalogAction($catalog, $page = 1)
    {

        if(empty($page)) {
            $page = 1;
        }

        $cat = $this->get('lego_category')->getCategoryByName($catalog);

        $cat = $cat[0]; // вообще не изящное решение );
        $categories = $this->get('lego_category')->getCategories();
        $orderList = $this->get('lego_product')->sortKeys;
        $productsData = $this->get('lego_product')->getProductsByCat($cat['c_id'], $page);
        $products = $productsData['products'];
        $pager = $productsData['pager'];
        $currencies = $this->get('lego_product')->getCurrencies();

        return $this->render(
            'LegoCatalogBundle:Listing:products.html.twig', array(
                'cat' => $cat,
                'data_prod' => $products,
                'pager_prod' => $pager,
                'categories' => $categories,
                'orderList' => $orderList,
                'currencies' => $currencies
            )
        );
    }

}