<?php
namespace Lego\CatalogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContextInterface;

use Lego\CatalogBundle\Services\LegoCategory;

class AdminController extends Controller {

    public function loginAction(Request $request) {

        if(true === $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')) {

            return $this->redirect($this->generateUrl('manage_catalog'));

        }

        $session = $request->getSession();

        // get the login error if there is one
        if ($request->attributes->has(SecurityContextInterface::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(
                    SecurityContextInterface::AUTHENTICATION_ERROR
            );
        } elseif (null !== $session && $session->has(SecurityContextInterface::AUTHENTICATION_ERROR)) {
            $error = $session->get(SecurityContextInterface::AUTHENTICATION_ERROR);
            $session->remove(SecurityContextInterface::AUTHENTICATION_ERROR);
        } else {
            $error = '';
        }

        // last username entered by the user
        $lastUsername = (null === $session) ? '' : $session->get(SecurityContextInterface::LAST_USERNAME);

        return $this->render(
            'LegoCatalogBundle:Admin:login.html.twig', array(
                'last_username' => $lastUsername,
                'error' => $error,
            )
        );
    }

    public function catalogAction() {

        $categories = $this->get('lego_category')->getCategories();

        return $this->render(
            'LegoCatalogBundle:Admin:admin.html.twig', array(
                'title' => 'Catalog', 'data' => $categories
            )
        );

    }

    public function formAction( $action, $type, $id = null ) {

        echo $action . '/' . $type . '/' . $id;

        die;

    }

}
