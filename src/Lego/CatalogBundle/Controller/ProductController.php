<?php
namespace Lego\CatalogBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\ORM\QueryBuilder;

class ProductController extends Controller
{

    public function showProductAction($name) {

        $product = $this->get('lego_product')->getProductByName($name);

        $id_product = $product->getId();
        $id_category = $product->getIdCategory();

        $product_attributes = $this->get('lego_product')->getProductAttributes($id_product);
        $product_images = $this->get('lego_product')->getProductImages($id_product);
        $category = $this->get('lego_category')->getCategoryById($id_category);
        $category = $category[0]; // Знову каюсь за бидлофікси
        $currencies = $this->get('lego_product')->getCurrencies();

        return $this->render(
            'LegoCatalogBundle:Product:product.html.twig', array(
                'data' => $product,
                'images' => $product_images,
                'attributes' => $product_attributes,
                'category' => $category,
                'currencies' => $currencies)
        );
    }

}