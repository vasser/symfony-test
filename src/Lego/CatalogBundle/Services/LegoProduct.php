<?php

namespace Lego\CatalogBundle\Services;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\Tools\Pagination\Paginator;

class LegoProduct extends \Knp\Bundle\PaginatorBundle\Definition\PaginatorAware
{
    /**
     * Entity Manager instance
     * @var object
     */
    protected $em;

    /**
     * Session instance
     * @var object
     */
    protected $sess;

    /**
     * Name of sorting
     * @var string
     */
    protected $sort;

    /**
     * Part of SQL query that creates sorting
     * @var string
     */
    protected $sortQuery;

    /**
     * List of valid sorting parameters
     * @var array
     */
    public $sortKeys = array(
        'cheap' => array('Price: lowest first', 'price ASC'), 'expensive' => array('Price: highest first', 'price DESC'),
        'newest' => array('Date: newest first', 'id DESC'), 'oldest' => array('Date: elder first', 'id ASC')
    );

    private $_perPage = 9;

    public function __construct( EntityManager $entityManager ) {
        $this->em = $entityManager;

        $session = new Session();
        $this->sess = $session;

        $this->getSortParam();
    }

    /**
     * Get list of all products
     * @param type $category
     * @return array
     */
    public function getProductsAll($page) {

        $rsm = new ResultSetMapping();
        $query = $this->em
                ->createQuery( $this->getBaseSQLList() . $this->sortQuery, $rsm)
                ->setFirstResult( $this->_perPage * ($page - 1) )
                ->setMaxResults( $this->_perPage );
        $products = $query->getScalarResult();
        $pagination = new Paginator($query);

        $data = array(
            'products' => $products,
            'pager' => $this->preparePagerData($page, $pagination)
        );

        return $data;

    }

    /**
     * Get list of products by category ID
     * @param type $category
     * @return array
     */
    public function getProductsByCat($category = null, $page) {

        $rsm = new ResultSetMapping();
        $query = $this->em
                ->createQuery( $this->getBaseSQLList() . "
                        WHERE p.idCategory = ?1
                        " . $this->sortQuery , $rsm)
                ->setParameter(1, $category)
                ->setFirstResult( $this->_perPage * ($page - 1) )
                ->setMaxResults( $this->_perPage );
        $products = $query->getScalarResult();
        $pagination = new Paginator($query);

        $data = array(
            'products' => $products,
            'pager' => $this->preparePagerData($page, $pagination)
        );

        return $data;

    }

    /**
     * Get products by url query
     * @param type $name_url
     * @return array
     */
    public function getProductByName($name_url) {

        $rsm = new ResultSetMapping();
        $query = $this->em
                ->createQuery( $this->getBaseSQLSingle() . "
                        WHERE p.nameUrl = ?1" , $rsm)
                ->setParameter(1, $name_url);

        $products = $query->getSingleResult();

        return $products;

    }

    /**
     * Returns base SQL to avoid code duplication in similar methods
     * @return string
     */
    private function getBaseSQLList() {

        return "SELECT p, pi.img, c.name, c.nameUrl
                FROM LegoCatalogBundle:Product p
                LEFT JOIN LegoCatalogBundle:ProductImages pi WITH pi.idProduct = p.id AND pi.main = '1'
                LEFT JOIN LegoCatalogBundle:Category c WITH c.id = p.idCategory";

    }

    /**
     * Returns base SQL to avoid code duplication in similar methods
     * @return string
     */
    private function getBaseSQLSingle() {

        return "SELECT p
                FROM LegoCatalogBundle:Product p";

    }

    /**
     * Get main currency
     * @return object
     */
    public function getCurrencies() {

        $rsm = new ResultSetMapping();
        $query = $this->em
                ->createQuery("SELECT c
                            FROM LegoCatalogBundle:Currency c
                            ORDER BY c.main DESC", $rsm);

        $currency = $query->getScalarResult();

        return $currency;

    }

    /**
     * Get list of product's attributes
     * @param type $id_product
     * @return array
     */
    public function getProductAttributes($id_product) {

        $p = $this->em
                ->getRepository('LegoCatalogBundle:ProductAttributes');

        $query = $p->createQueryBuilder('pa')
                ->select('pa.pkey, pa.pvalue')
                ->where('pa.idProduct = ?1')
                ->setParameter(1, $id_product);

        return $query->getQuery()->getResult();
    }

    /**
     * Get list of product's images
     * @param type $id_product
     * @return array
     */
    public function getProductImages($id_product) {

        $p = $this->em
                ->getRepository('LegoCatalogBundle:ProductImages');

        $query = $p->createQueryBuilder('pi')
                ->select('pi.img')
                ->where('pi.idProduct = ?1')
                ->setParameter(1, $id_product);

        return $query->getQuery()->getResult();
    }

    function getPagination($dql, $page) {
        $rsm = new ResultSetMapping();
        $query = $this->em
                ->createQuery( $dql, $rsm)
                ->setFirstResult( $this->_perPage * ($page - 1) )
                ->setMaxResults( $this->_perPage );
        $paginator = new Paginator($query);

        $c = count($paginator);

        return $paginator;
    }

    function prepareSortParam($sort) {
        $this->sort = $sort;
        $this->sortQuery = " ORDER BY p." . $this->sortKeys[$sort][1];
    }

    function saveSortParam($sort) {

        $this->sess->set('sort', $sort);

    }

    function getSortParam() {

        $request = new Request( $_GET );
        $sort = null;
        $sortGet = $request->query->get('sort');
        if(empty($sortGet)) {
            $sort = $this->sess->get('sort');
        } else {
            $sort = $sortGet;
        }

        if(is_null($sort) === false && array_key_exists($sort, $this->sortKeys)) {
            $this->prepareSortParam($sort);
            $this->saveSortParam($sort);
        }

    }

    /**
     * Prepares array to generate paginator
     * @param type $pagination Pagination instance
     * @return array
     */
    function preparePagerData($page, $pagination) {

        return array(
            'itemsTotal' => count($pagination),
            'pageCurrent' => $page,
            'pagesTotal' => ceil(count($pagination) / $this->_perPage)
        );

    }

}