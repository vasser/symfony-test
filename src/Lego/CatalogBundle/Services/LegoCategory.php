<?php

namespace Lego\CatalogBundle\Services;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\ResultSetMapping;

class LegoCategory {

    protected $em;

    public function __construct( EntityManager $entityManager ) {
        $this->em = $entityManager;
    }

    /**
     * Get list of categories
     * @return array
     */
    public function getCategories() {
        $rsm = new ResultSetMapping();
        $categories = $this->em
                ->createQuery( $this->getBaseSQL() . "
                        GROUP BY c.id", $rsm)
                ->getScalarResult();
        return $categories;
    }

    /**
     * Get single category data by its ID
     * @param type $name_category Category ID
     * @return array
     */
    public function getCategoryByName($name_category) {
        $rsm = new ResultSetMapping();
        $categories = $this->em
                ->createQuery( $this->getBaseSQL() . "
                        WHERE c.nameUrl = ?1
                        GROUP BY c.id", $rsm)
                ->setParameter(1, $name_category)
                ->getScalarResult();
        return $categories;
    }

    /**
     * Get single category data by its ID
     * @param type $name_category Category ID
     * @return array
     */
    public function getCategoryById($id_category) {
        $rsm = new ResultSetMapping();
        $categories = $this->em
                ->createQuery( $this->getBaseSQL() . "
                        WHERE c.id = ?1
                        GROUP BY c.id", $rsm)
                ->setParameter(1, $id_category)
                ->getScalarResult();
        return $categories;
    }

    /**
     * Returns base SQL to avoid code duplication in similar methods
     * @return string
     */
    private function getBaseSQL() {

        return "SELECT  c, COALESCE( COUNT(p.id), '0' ) products
                FROM LegoCatalogBundle:Category c
                LEFT JOIN LegoCatalogBundle:Product p WITH p.idCategory = c.id";

    }

}